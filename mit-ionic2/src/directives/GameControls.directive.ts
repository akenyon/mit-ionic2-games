import {Directive, ElementRef, Output, OnInit, OnDestroy, EventEmitter} from '@angular/core'
import {Gesture} from 'ionic-angular/gestures/gesture'
declare var Hammer: any

@Directive({
  selector: '[game-controls]' // Attribute selector
})
export class GameControls implements OnInit, OnDestroy {
  @Output('swipe-action') swipeAction: EventEmitter<any> = new EventEmitter();
  @Output('tap-action') tapAction:EventEmitter<any> = new EventEmitter();

  private el: HTMLElement
  private gestures: Gesture

  constructor(el: ElementRef) {
    this.el = el.nativeElement
  }

  ngOnInit() {
    this.gestures = new Gesture(this.el, {
      recognizers: [
        [Hammer.Swipe, {direction: Hammer.DIRECTION_ALL}],
        [Hammer.Tap]
      ]
    });
    this.gestures.listen()
    this.gestures.on('swipe', e => {
      this.swipeAction.emit(e);
    });
    this.gestures.on('tap', e => {
      this.tapAction.emit(e);
    })
  }

  ngOnDestroy() {
    this.gestures.destroy();
  }
}
