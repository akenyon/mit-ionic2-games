import { Component, OnInit, HostListener } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'snake-page',
  templateUrl: 'snake.page.html'
})
export class SnakePage implements OnInit {
  _gameStatusEnum:any = GameStatus;
  _gridSize:number = 25;
  _items:any[][] = [];
  _gameStatus:GameStatus = GameStatus.WaitingToStart;
  _currentX:number = 5;
  _currentY:number = 12;
  _previousHeading:number = 4;
  _heading:number = 4;
  _edge:string = "25px";
  _intervalSpeed:number = 250;
  _interval:number;
  _snakeLength:number = 2;
  _points:number = 0;
  _multiplier:number = 1;

  constructor(public navCtrl: NavController) {}

  public ngOnInit():void {
    this.BuildStartingGrid();
  }

  private BuildStartingGrid():void {
    // Build the grid items
    for (let i = 0; i < this._gridSize; i++) {
        this._items[i] = [];
        for (let j = 0; j < this._gridSize; j++) {
            this._items[i][j] = this.CreateEmptySquare();
        }
    }

    // create the snake
    this._items[this._currentY][this._currentX] = this.CreateSnakeSquare(2);
    this._items[this._currentY][this._currentX - 1] = this.CreateSnakeSquare(1);

    // create the fruit? or is it a mouse?
    this.CreateTarget();
  }

  Start():void {
    this._intervalSpeed = 250;
    this._points = 100;
    this._gameStatus = GameStatus.Running;
    // start intervals
    this.StartIntervals();
  }

  Restart():void {
    this.BuildStartingGrid();
    this.Start();
  }

  Pause():void {
    clearInterval(this._interval);
    this._gameStatus = GameStatus.Paused;
  }

  Continue():void {
    this.StartIntervals();
    this._gameStatus = GameStatus.Running;
  }

  StartIntervals():void {
    this._interval = setInterval(x => this.RefreshGame(), this._intervalSpeed - (this._multiplier * 50));
  }

  Swipe(event:any):void {
    this.SetHeading(event.direction);
  }

  Tap(event:any):void {
    if (event.tapCount > 1) {
        switch(this._gameStatus){
          case GameStatus.WaitingToStart:
            this.Start();
            break;
          case GameStatus.GameOver:
            this.Restart();
            break;
          case GameStatus.Running:
            this.Pause();
            break;
          case GameStatus.Paused:
            this.Continue();
            break;
        }
    }
  }

  private RefreshGame():void {
    let nextSquare = this.GetNextSquare();
    if (nextSquare == null) {
      return;
    } else if (nextSquare.type == 'target') {
      // get some points
      this._points += 100;

      if ((this._points / 100) % 5 > this._multiplier - 1) {
          this._multiplier = (this._points / 500) + 1;
          // speed up the game
          clearInterval(this._interval);
          this.StartIntervals();
      }

      //lengthen the snake
      this._snakeLength += 1;
      //convert it to a snake
      this._items[nextSquare.y][nextSquare.x] = this.CreateSnakeSquare(this._snakeLength);
      //create a new target
      this.CreateTarget();
    } else {
      //convert it to a snake
      this._items[nextSquare.y][nextSquare.x].type = 'snake';
      this._items[nextSquare.y][nextSquare.x].index = this._snakeLength + 1;
      //decrement snake items
      this._items.forEach(row => {
        row.forEach(item => {
          if (item.type == 'snake') {
              item.index -= 1;
              if (item.index == 0) {
                  item.type = null;
              }
          }
        });
      });
    }
    //update previous heading
    this._previousHeading = this._heading;
  }

  private GetNextSquare():any {
    //find the snake head
    let x,y,index = 0;

    for (let i = 0; i < this._items.length; i++) {
        for(let j = 0; j< this._items[i].length; j++){
          if (this._items[i][j].type == 'snake' && this._items[i][j].index > index) {
              x = j;
              y = i;
              index = this._items[i][j].index;
          }
        }
    }

    // find the next coordinates
    switch(this._heading){
      case 2:
        x -=1;
        break;
      case 4:
        x += 1;
        break;
      case 8:
        y -= 1;
        break;
      case 16:
        y += 1;
        break;
    }

    if (x < 0 || x >= this._gridSize || y < 0 || y >= this._gridSize || this._items[y][x].type == 'snake') {
        this._gameStatus = GameStatus.GameOver;
        clearInterval(this._interval);
        return null;
    }

    return { x:x, y:y, type:this._items[y][x].type };
  }

  private CreateTarget():void {
    let locationIsGood = false;

    while (!locationIsGood) {
      let x = Math.round(Math.random() * (this._gridSize - 1));
      let y = Math.round(Math.random() * (this._gridSize -1 ));

      if (this._items[y][x].type != 'snake') {
          this._items[y][x].type = 'target';
          locationIsGood = true;
      }
    }
  }

  private CreateEmptySquare():any {
    return {
      'type': null
    };
  }

  private CreateSnakeSquare(index:number):any {
    return {
      'type': 'snake',
      'index': index
    }
  }

  private SetHeading(nextHeading:number):void {
    if ((this._previousHeading == 2 && nextHeading != 4) ||
        (this._previousHeading == 4 && nextHeading != 2) ||
        (this._previousHeading == 8 && nextHeading != 16) ||
        (this._previousHeading == 16 && nextHeading != 8)) {
        this._heading = nextHeading;
    }
  }

  // keyboard events
  @HostListener('document:keydown',['$event'])
  KeyPress(event:KeyboardEvent):void {
    switch(event.key) {
      case "ArrowUp":
        this.SetHeading(8);
        break;
      case "ArrowDown":
        this.SetHeading(16);
        break;
      case "ArrowRight":
        this.SetHeading(4);
        break;
      case "ArrowLeft":
        this.SetHeading(2);
        break;
      case "Enter":
        if (this._gameStatus != GameStatus.Running) {
          this.Start();
        }
        break;
      default:
        break;
    }
  }
}

enum GameStatus {
  WaitingToStart = 1,
  Running = 2,
  GameOver = 4,
  Paused = 8
}
